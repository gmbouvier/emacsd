(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (web-mode ag helm-system-packages nord-theme nim-mode clojure-mode clojurescript-mode racer cargo rust-mode hydra helm-projectile projectile-ripgrep projectile helm-ag inf-ruby flycheck chruby ruby-end enh-ruby-mode beacon clipmon company company-quickhelp diff-hl diminish easy-kill evil evil-magit evil-nerd-commenter expand-region helm indent-guide key-chord magit multiple-cursors no-littering persistent-scratch region-bindings-mode restart-emacs smart-mode-line smartparens which-key whitespace-cleanup-mode workgroups2))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide 'grm-custom)
