(require 'grm-misc-loader)

(defvar grm-enabled-misc-settings-list
  '(
    auto-insert
    beacon-mode
    clipmon
    company-mode
    diff-hl
    diminish
    emacs-defaults
    evil-plugins
    expand-region
    helm
    hydra
    grm-leader-mode
    indent-guide
    key-chord-mode
    magit
    performance
    persistent-scratch
    projectile
    smart-mode-line
    smartparens
    subword-mode
    which-key
    whitespace-cleanup
    workgroups

    defuns
    bindings
    ))

(mapc 'grm-misc-enable-setting grm-enabled-misc-settings-list)

(provide 'grm-misc)
