(require 'chruby)
(chruby "2.6.5")

(setq inf-ruby-default-implementation "pry"
      inf-ruby-first-prompt-pattern "^\\[[0-9]+\\] pry\\((.*)\\)> *"
      inf-ruby-prompt-pattern "^\\[[0-9]+\\] pry\\((.*)\\)[>*\"'] *")
(add-hook 'enh-ruby-mode-hook 'inf-ruby-minor-mode)

(provide 'grm-ruby)
