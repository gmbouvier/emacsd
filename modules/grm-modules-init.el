(grm-require 'grm-visual "20modules")
(grm-require 'grm-mode "20modules")
(grm-require 'grm-leader-mode "20modules")
(grm-require 'grm-evil "20modules")
(grm-require 'grm-misc "20modules")
(grm-require 'grm-programming "30programming")

(provide 'grm-modules-init)
