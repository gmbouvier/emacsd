(grm-define-highest-priority-mode-function grm-leader-local-mode)
(defun grm-grm-leader-mode-enable-hook ()
  (grm-gain-highest-keys-priority-grm-leader-local-mode nil)
  (setq cursor-type 'hollow))
(add-hook 'grm-leader-mode-enabled-hook 'grm-grm-leader-mode-enable-hook)
(defun grm-grm-leader-mode-disable-hook ()
  (setq cursor-type 'bar))
(add-hook 'grm-leader-mode-disabled-hook 'grm-grm-leader-mode-disable-hook):

(provide 'grm-misc-grm-leader-mode)
