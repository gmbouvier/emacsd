(beacon-mode +1)
(dolist (mode '(eshell-mode term-mode))
  (add-to-list 'beacon-dont-blink-major-modes mode))

(provide 'grm-misc-beacon-mode)
