(dolist (func '(er/mark-inside-pairs
                  er/mark-inside-quotes
                  er/mark-outside-pairs
                  er/mark-outside-quotes
                  er/mark-defun
                  er/mark-comment
                  er/mark-text-sentence
                  er/mark-text-paragraph
                  er/mark-word
                  er/mark-url
                  er/mark-email
                  er/mark-symbol))
  (autoload func "expand-region"))

(provide 'grm-misc-expand-region)
