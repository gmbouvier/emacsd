(with-eval-after-load 'magit
  (add-hook 'magit-popup-mode-hook
            (lambda () (setq show-trailing-whitespace nil))))
(require 'evil-magit)

(provide 'grm-misc-magit)
