(key-chord-mode +1)
(setq key-chord-two-keys-delay 0.3)
(fset 'key-chord-define 'grm-key-chord-define)

(provide 'grm-misc-key-chord-mode)
