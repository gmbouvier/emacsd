;; Leader mode
(setq grm-leader-special '(?q ?w ?t ?y ?i ?o
                           ?d ?f ?g ?j
                           ?z ?v ?b))
(defcustom grm-leader-mod-alist
  '((nil . "C-")
    ("," . "C-M-")
    ("m" . "M-"))
  "List of keys and their associated modifer."
  :group 'grm-leader
  :type '(alist))

;; Nested Maps
(define-prefix-command 'ctrl-c-q-map)   ; quit
                                        ; leader-w - evil-window-map
(define-prefix-command 'ctrl-c-f-map)   ; leader-f-m -> multiple cursors map
(define-prefix-command 'ctrl-c-g-map)   ; leader-p -> git
                                        ; leader-p -> projectile-command-map
(define-key grm-mode-map (kbd "C-c q") 'ctrl-c-q-map)
(define-key grm-mode-map (kbd "C-c w") 'evil-window-map)
(define-key grm-mode-map (kbd "C-c f") 'ctrl-c-f-map)
(define-key grm-mode-map (kbd "C-c g") 'ctrl-c-g-map)
(define-key grm-mode-map (kbd "C-c v") 'projectile-command-map)

;; company
(define-key           grm-mode-map (kbd "C-<tab>") 'company-complete)
(grm-key-chord-define company-active-map "jk"        'company-complete)
(define-key           company-active-map (kbd "C-j") 'company-select-next)
(define-key           company-active-map (kbd "C-k") 'company-select-previous)
(define-key           company-active-map [escape] 'keyboard-quit)

;; editing
(define-key grm-mode-map (kbd "M-w")   'easy-kill)
(define-key evil-normal-state-map "gc" 'evilnc-comment-operator)

;; emacs
(define-key ctrl-c-q-map (kbd "R") 'restart-emacs)
(define-key ctrl-c-g-map (kbd "g") 'keyboard-quit)

;; evil
(define-key           evil-insert-state-map (kbd "<escape>") 'evil-normal-state)
(grm-key-chord-define evil-insert-state-map "jj"             'evil-normal-state)
(global-set-key [escape]          'evil-exit-emacs-state)
(define-key grm-mode-map [escape] 'minibuffer-keyboard-quit)
; (define-key multiple-cursors-mode-map [escape] 'mc/keyboard-quit)

;; helm
(define-key grm-mode-map        (kbd "M-x")     'helm-M-x)
(define-key grm-mode-map        (kbd "C-x C-f") 'helm-find-files)
(define-key grm-mode-map        (kbd "C-x C-b") 'helm-mini)
(define-key grm-mode-map        (kbd "C-x C-y") 'helm-show-kill-ring)
(define-key helm-map            (kbd "<tab>")   'helm-execute-persistent-action)
(define-key helm-find-files-map [C-backspace]   'helm-find-files-up-one-level)
(define-key helm-find-files-map (kbd "C-h")     'helm-find-files-up-one-level)
(define-key helm-map            (kbd "C-j")     'helm-next-line)
(define-key helm-map            (kbd "C-k")     'helm-previous-line)
(define-key helm-find-files-map (kbd "C-l")     'helm-execute-persistent-action)
(define-key helm-map            (kbd "C-z")     'helm-select-action)
(with-eval-after-load 'grm-leader-mode
  (define-key grm-leader-local-mode-map (kbd "SPC") 'helm-mini))

;; git
(define-key ctrl-c-g-map "[" #'diff-hl-previous-hunk)
(define-key ctrl-c-g-map "]" #'diff-hl-next-hunk)
(define-key ctrl-c-g-map "s" #'magit-status)

;; grm-leader
(define-key grm-mode-map          (kbd "C-x C-1") 'delete-other-windows)
(define-key grm-mode-map          (kbd "C-x C-2") 'split-window-below)
(define-key grm-mode-map          (kbd "C-x C-3") 'split-window-right)
(define-key grm-mode-map          (kbd "C-x C-0") 'delete-window)
(define-key grm-mode-map          (kbd "M-j") 'evil-execute-in-grm-leader-state)
(define-key evil-normal-state-map (kbd "SPC") 'evil-execute-in-grm-leader-state)
(define-key evil-visual-state-map (kbd "SPC") 'evil-execute-in-grm-leader-state)
(define-key evil-motion-state-map (kbd "SPC") 'evil-execute-in-grm-leader-state)
(define-key magit-mode-map        (kbd "SPC") 'evil-execute-in-grm-leader-state)
(grm-key-chord-define evil-insert-state-map "kk"
                      'evil-execute-in-grm-leader-state)

;; multiple cursors
(define-key ctrl-c-f-map "ma" 'mc/mark-all-like-this)
(define-key ctrl-c-f-map "mp" 'mc/mark-previous-like-this)
(define-key ctrl-c-f-map "mn" 'mc/mark-next-like-this)
(define-key ctrl-c-f-map "me" 'mc/edit-lines)
(define-key ctrl-c-f-map "ms" 'mc/mark-all-symbols-like-this)
(define-key ctrl-c-f-map "mS" 'mc/mark-all-symbols-like-this-in-defun)
(define-key ctrl-c-f-map "md" 'mc/mark-all-like-this-dwim)
(define-key ctrl-c-f-map "m>" 'mc/skip-to-next-like-this)
(define-key ctrl-c-f-map "m<" 'mc/skip-to-previous-like-this)
(define-key grm-mode-map (kbd "M-S-<mouse-1>") 'mc/add-cursor-on-click)

;; selection
(define-key grm-mode-map (kbd "C-=") 'er/expand-region)

;; which-key
(define-key grm-mode-map (kbd "C-c ?") 'which-key-show-top-level)
(define-key grm-mode-map (kbd "C-c /") 'grm-which-key-show-major-mode-keymap)

(provide 'grm-misc-bindings)
