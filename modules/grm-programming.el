(add-to-list 'load-path (concat grm-modules-dir "/programming"))

(grm-require 'grm-javascript "30programming")
(grm-require 'grm-python "30programming")
(grm-require 'grm-ruby "30programming")
(grm-require 'grm-rust "30programming")
(grm-require 'grm-web "30programming")

(provide 'grm-programming)
